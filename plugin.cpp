#include "plugin.h"

Plugin::Plugin() :
	QObject(nullptr),
	PluginBase(this)
	, m_GUIElementBase(new GUIElementBase(this, {"MainMenuItem"}, "qrc:/UserTasksCalendarView.qml"))
{
	initPluginBase(
	{
		{INTERFACE(IPlugin), this},
		{INTERFACE(IGUIElement), m_GUIElementBase}
	},
	{
		{INTERFACE(IUserTasksCalendar), m_userTasksCalendar}
	}
	);
	m_GUIElementBase->initGUIElementBase(
		{{"taskCalendar", m_userTasksCalendar.reference()}}
	);
	connect(m_GUIElementBase, &GUIElementBase::onOpened, [=]() {
		QMetaObject::invokeMethod(m_GUIElementBase->rootObject(), "update");
	});
}

Plugin::~Plugin()
{
}
