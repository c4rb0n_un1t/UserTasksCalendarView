#pragma once

#include <QtCore>

#include "../../Interfaces/Architecture/PluginBase/plugin_base.h"
#include "../../Interfaces/Architecture/GUIElementBase/guielementbase.h"

#include "../../Interfaces/Utility/iusertaskscalendar.h"


class Plugin : public QObject, public PluginBase
{
	Q_OBJECT
	Q_PLUGIN_METADATA(IID "MASS.Module.UserTasksCalendarView" FILE "PluginMeta.json")
	Q_INTERFACES(IPlugin)

public:
	Plugin();
	~Plugin() override;

private:
	GUIElementBase* m_GUIElementBase;
	ReferenceInstancePtr<IUserTasksCalendar> m_userTasksCalendar;
};
