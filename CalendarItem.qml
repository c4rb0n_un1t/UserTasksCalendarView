import QtQuick 2.12
import QtQuick.Controls 2.12
//import QtQuick.Controls.Private 1.0
import QtQuick.Layouts 1.12

Item {
    id: calendarComponent
    
	
	function updateCalendar () {
		calendar.showPreviousMonth()
		calendar.showNextMonth()
	}
    

    SystemPalette {
        id: systemPalette
    }

    Flow {
        id: row
        anchors.fill: parent
        anchors.margins: 20
        spacing: 10
        layoutDirection: Qt.RightToLeft

//        Calendar {
//            id: calendar
//            width: (parent.width > parent.height ? parent.width * 0.6
//                                                   - parent.spacing : parent.width)
//            height: (parent.height > parent.width ? parent.height * 0.6
//                                                    - parent.spacing : parent.height)
//            frameVisible: true
//            weekNumbersVisible: true
//            focus: true

//            style: CalendarStyle {
//                navigationBar: Rectangle {
//                    height: Math.round(TextSingleton.implicitHeight * 2.73)
//                    color: systemPalette.window

//                    Rectangle {
//                        color: Qt.rgba(1, 1, 1, 0.6)
//                        height: 1
//                        width: parent.width
//                    }

//                    Rectangle {
//                        anchors.bottom: parent.bottom
//                        height: 1
//                        width: parent.width
//                        color: systemPalette.window
//                    }

//                    HoverButton {
//                        id: previousMonth
//                        width: parent.height
//                        height: width
//                        anchors.verticalCenter: parent.verticalCenter
//                        anchors.left: parent.left
//                        source: "qrc:/res/back.png"
//                        onClicked: control.showPreviousMonth()
//                    }
//                    Label {
//                        id: dateText
//                        text: styleData.title
//                        elide: Text.ElideRight
//                        horizontalAlignment: Text.AlignHCenter
//                        font.pixelSize: TextSingleton.implicitHeight * 1.25
//                        anchors.verticalCenter: parent.verticalCenter
//                        anchors.left: previousMonth.right
//                        anchors.leftMargin: 2
//                        anchors.right: nextMonth.left
//                        anchors.rightMargin: 2
//                    }
//                    HoverButton {
//                        id: nextMonth
//                        width: parent.height
//                        height: width
//                        anchors.verticalCenter: parent.verticalCenter
//                        anchors.right: parent.right
//                        source: "qrc:/res/next.png"
//                        onClicked: control.showNextMonth()
//                    }
//                }

//                dayOfWeekDelegate: Rectangle {
//                    implicitHeight: 50
//                    color: systemPalette.window
//                    Label {
//                        text: control.locale.dayName(styleData.dayOfWeek,
//                                                     control.dayOfWeekFormat)
//                        anchors.centerIn: parent
//                    }
//                }

//                weekNumberDelegate: Rectangle {
//                    implicitWidth: 50
//                    color: systemPalette.window
//                    Label {
//                        text: styleData.weekNumber
//                        anchors.centerIn: parent
//                    }
//                }

//                background: Rectangle {
//                    color: systemPalette.window
//                }

//                dayDelegate: Item {
//                    readonly property color sameMonthDateTextColor: systemPalette.text
//                    readonly property color selectedDateColor: "#81a1c1"
//                    readonly property color selectedDateTextColor: systemPalette.highlightedText
//                    readonly property color differentMonthDateTextColor: "#4c566a"
//                    readonly property color invalidDatecolor: systemPalette.alternateBase
//					property var tasks: styleData.date !== undefined &&
//					taskCalendarObject !== null && taskCalendarObject !== undefined ? taskCalendarObject.getTasksForDay(styleData.date).tasks : null
//					property var tasksCount: tasks !== null ? tasks.length : 0

//                    Rectangle {
//                        anchors.fill: parent
//                        border.color: systemPalette.window
//                        color: styleData.date !== undefined
//                               && styleData.selected ? selectedDateColor : systemPalette.window
//                        anchors.margins: styleData.selected ? -1 : 0
//                    }

//                    Rectangle {
//                        visible: tasksCount > 0
//                        anchors.top: parent.top
//                        anchors.left: parent.left
//                        anchors.leftMargin: 1
//						anchors.topMargin: 1
//                        width: 20
//                        height: width
//                        color: "#d8dee9"
//                        radius: 90
//                        Label {
//							anchors.fill: parent
//							text: tasksCount
//							color: systemPalette.window
//							horizontalAlignment: Text.AlignHCenter
//							verticalAlignment: Text.AlignVCenter
//                        }
//                    }

//                    Label {
//                        id: dayDelegateText
//                        text: styleData.date.getDate()
//                        anchors.centerIn: parent
//                        color: {
//                            var color = invalidDatecolor
//                            if (styleData.valid) {
//                                // Date is within the valid range.
//                                color = styleData.visibleMonth ? sameMonthDateTextColor : differentMonthDateTextColor
//                                if (styleData.selected) {
//                                    color = selectedDateTextColor
//                                }
//                            }
//                            color
//                        }
//                    }
//                }
//            }
//        }

        Component {
            id: eventListHeader

            Row {
                id: eventDateRow
                width: parent.width
                height: eventDayLabel.height
                spacing: 10

                Label {
                    id: eventDayLabel
                    text: calendar.selectedDate.getDate()
                    font.pointSize: 35
                }

                Label {
                    readonly property var options: {
                        weekday: "long"
                    }
                    text: Qt.locale().standaloneDayName(
                              calendar.selectedDate.getDay(), Locale.LongFormat)
                    font.pointSize: 18
                }
                Label {
                    text: Qt.locale().standaloneMonthName(
                              calendar.selectedDate.getMonth(
                                  )) + calendar.selectedDate.toLocaleDateString(
                              Qt.locale(), " yyyy")
                    font.pointSize: 18
                }
            }
        }

        Rectangle {
            width: (parent.width > parent.height ? parent.width * 0.4
                                                   - parent.spacing : parent.width)
            height: (parent.height > parent.width ? parent.height * 0.4
                                                    - parent.spacing : parent.height)
            color: systemPalette.window
            border.color: "#d8dee9"
            
            ListModel {
				id: emptyModel
            }

            ListView {
                id: eventsListView
                spacing: 4
                clip: true
                header: eventListHeader
                anchors.fill: parent
                anchors.margins: 5
                model: taskCalendarObject !== null && taskCalendarObject !== undefined ? 
					taskCalendarObject.getTasksForDay(calendar.selectedDate).tasks : emptyModel

                delegate: Rectangle {
                    width: eventsListView.width
                    height: eventItemColumn.height
                    color: systemPalette.window
                    border.color: "#d8dee9"

                    Column {
                        id: eventItemColumn
                        anchors.margins: 8
                        spacing: 8
                        anchors.left: parent.left
                        anchors.right: parent.right
                        height: timeLabel.height + nameLabel.height + 8

                        Label {
                            id: nameLabel
                            width: parent.width
                            wrapMode: Text.Wrap
							horizontalAlignment: Text.AlignLeft
							verticalAlignment: Text.AlignBottom
                            text: modelData.name
                        }
                        Label {
                            id: timeLabel
                            width: parent.width
                            wrapMode: Text.Wrap
							horizontalAlignment: Text.AlignLeft
							verticalAlignment: Text.AlignBottom
                            text: "Start time:" + modelData.startDate.toLocaleTimeString(calendar.locale, Locale.ShortFormat) + 
								(modelData.daysLeft > 0 ? " Days left: " + modelData.daysLeft : "")
                            color: systemPalette.text
                        }
                    }
                }
            }
        }
    }
}
